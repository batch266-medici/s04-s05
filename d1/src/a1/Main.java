package a1;

public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();

        Contact firstContact = new Contact("John Doe", "091234567891", "Caloocan, Philippines");

        Contact secondContact = new Contact("Jane Doe", "092345678912", "Taguig, Philippines");

        phonebook.setContacts(firstContact);
        phonebook.setContacts(secondContact);

        if(phonebook.getContacts().isEmpty()) {
            System.out.println("The phonebook is empty.");
        } else {
            phonebook.getContacts().forEach((contact -> printDetails(contact)));
        }

    }

    public static void printDetails(Contact contact) {
        System.out.println(contact.getName());
        if (contact.getContactNumber().isEmpty()) {
            System.out.println(contact.getName() + "has no contact number.");
        } else {
            System.out.println(contact.getContactNumber());
        }
        if (contact.getAddress().isEmpty()) {
            System.out.println(contact.getName() + "has no address");
        } else {
            System.out.println(contact.getAddress());
        }
    }
}
