package a1;

import java.util.ArrayList;


public class Phonebook {
    private ArrayList<Contact> contacts = new ArrayList<Contact>();

    /*Constructor*/
    public Phonebook() {
        contacts = new ArrayList<Contact>();
    }

    public Phonebook(Contact contact) {
        this.contacts = contacts;
    }

    /*Setter*/
    public void setContacts(Contact contacts) {
        this.contacts.add(contacts);
    }

    /*Getter*/
    public ArrayList<Contact> getContacts() {
        return contacts;
    }

}
