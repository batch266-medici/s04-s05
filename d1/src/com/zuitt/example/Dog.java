package com.zuitt.example;

public class Dog extends Animal {
    private String breed;

    public Dog() {
        super(); /*Animal() Constructors*/
        this.breed = "Fenrir";
    }

    public Dog(String name, String color, String breed) {
        super(name, color); /*Animal(String name, String color) -> constructor */
        this.breed = breed;
    }

    /*Getter*/
    public String getBreed() {
        return this.breed;
    }

    /*Methods*/
    public void speak() {
        super.call(); /*The call() method from Animal Class*/
        System.out.println("Woof Woof!");
    }
}
